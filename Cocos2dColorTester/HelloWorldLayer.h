//
//  HelloWorldLayer.h
//  Cocos2dColorTester
//
//  Created by Daniel Rodriguez on 4/25/13.
//  Copyright __GeekCirCus__ 2013. All rights reserved.
//


#import <GameKit/GameKit.h>

// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"
#import "Slider.h"

// HelloWorldLayer
@interface HelloWorldLayer : CCLayer <GKAchievementViewControllerDelegate, GKLeaderboardViewControllerDelegate>
{
    Slider *slider1, *slider2, *slider3;
    Slider *slider4;
    CCLayerColor *layerColor;
    ccColor4B color;
}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;

@end
