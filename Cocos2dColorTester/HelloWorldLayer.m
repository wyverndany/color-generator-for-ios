//
//  HelloWorldLayer.m
//  Cocos2dColorTester
//
//  Created by Daniel Rodriguez on 4/25/13.
//  Copyright __GeekCirCus__ 2013. All rights reserved.
//


// Import the interfaces
#import "HelloWorldLayer.h"

// Needed to obtain the Navigation Controller
#import "AppDelegate.h"

#pragma mark - HelloWorldLayer

// HelloWorldLayer implementation
@implementation HelloWorldLayer

// Helper class method that creates a Scene with the HelloWorldLayer as the only child.
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	HelloWorldLayer *layer = [HelloWorldLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super's" return value
	if( (self=[super init]) ) {
    
		// ask director for the window size
		CGSize size = [[CCDirector sharedDirector] winSize];
        
        // create labels to indicate what each value means
        CCLabelTTF *label1 = [CCLabelTTF labelWithString:@"Red" fontName:@"Marker Felt" fontSize:20];
        CCLabelTTF *label2 = [CCLabelTTF labelWithString:@"Green" fontName:@"Marker Felt" fontSize:20];
        CCLabelTTF *label3 = [CCLabelTTF labelWithString:@"Blue" fontName:@"Marker Felt" fontSize:20];
        CCLabelTTF *label4 = [CCLabelTTF labelWithString:@"Opacity!" fontName:@"Marker Felt" fontSize:20];
		
        // create 4 sliders. One for each input value to generate a color
        // each slider has a label indicating its current value and a label to identify it
        // then all the items are added to the current layer
        
        slider1 = [[Slider alloc] initWithFrame:CGRectMake(50, 50 , 100, 20)];
        [slider1 addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
        [self addChild:slider1.sliderLabel z:50];
        [label1 setPosition:ccp(slider1.sliderLabel.position.x,
                               slider1.sliderLabel.position.y + label1.contentSize.height)];
        [self addChild:label1 z:50];
		[[[CCDirector sharedDirector]view] addSubview:slider1];
        
        slider2 = [[Slider alloc] initWithFrame:CGRectMake(150, 50 , 100, 20)];
        [slider2 addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
        [self addChild:slider2.sliderLabel z:50];
        [label2 setPosition:ccp(slider2.sliderLabel.position.x,
                                slider2.sliderLabel.position.y + label2.contentSize.height)];
        [self addChild:label2 z:50];
		[[[CCDirector sharedDirector]view] addSubview:slider2];
        
        slider3 = [[Slider alloc] initWithFrame:CGRectMake(250, 50 , 100, 20)];
        [slider3 addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
        [self addChild:slider3.sliderLabel z:50];
        [label3 setPosition:ccp(slider3.sliderLabel.position.x,
                                slider3.sliderLabel.position.y + label3.contentSize.height)];
        [self addChild:label3 z:50];
		[[[CCDirector sharedDirector]view] addSubview:slider3];
        
        slider4 = [[Slider alloc] initWithFrame:CGRectMake(350, 50 , 100, 20)];
        [slider4 addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
        [self addChild:slider4.sliderLabel z:50];
        [label4 setPosition:ccp(slider4.sliderLabel.position.x,
                                slider4.sliderLabel.position.y + label4.contentSize.height)];
        [self addChild:label4 z:50];
		[[[CCDirector sharedDirector]view] addSubview:slider4];
        
        // add button to create a color layer to display the current color selected
        
        CCMenuItemImage *createButton = [CCMenuItemImage itemWithNormalImage:@"button.png" selectedImage:@"button.png" target:self selector:@selector(createLayer:)];
		
		CCMenu *menu = [CCMenu menuWithItems: createButton, nil];
		[menu setPosition:ccp(size.width/2, size.height/2)];
		
		// Add the menu to the layer
		[self addChild:menu z:100];

	}
	return self;
}

-(void)sliderAction:(id)sender {
    
    color = ccc4([slider1 updateValue], [slider2 updateValue], slider3.updateValue, [slider4 updateValue]);
}

-(void)createLayer:(id)sender {
    // if there is another color layer set clean it.
    if (layerColor != nil) {
        [self removeChildByTag:10 cleanup:YES];
        layerColor = nil;
        [layerColor release];
    }
    
    // create the layer with the current parameters selecter in the sliders
    layerColor = [[CCLayerColor alloc] initWithColor:color];
    
    // add it behind everything else
    [self addChild:layerColor z:10 tag:10];
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
    slider1 = nil;
    [slider1 release];
    
	// don't forget to call "super dealloc"
	[super dealloc];
}

#pragma mark GameKit delegate

-(void) achievementViewControllerDidFinish:(GKAchievementViewController *)viewController
{
	AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
	[[app navController] dismissModalViewControllerAnimated:YES];
}

-(void) leaderboardViewControllerDidFinish:(GKLeaderboardViewController *)viewController
{
	AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
	[[app navController] dismissModalViewControllerAnimated:YES];
}
@end
