//
//  Slider.m
//  Cocos2dColorTester
//
//  Created by Daniel Rodriguez on 4/25/13.
//
//

#import "Slider.h"

@implementation Slider
@synthesize sliderLabel;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.value = 0;
        self.backgroundColor = [UIColor clearColor];
        self.value = 0;
        
        [self setMinimumValue:0];
        [self setMaximumValue:255];
        
        // init my label
        sliderLabel = [CCLabelTTF labelWithString:@"Value" fontName:@"Marker Felt" fontSize:20];
        [sliderLabel setPosition:ccp(self.frame.origin.x + self.frame.size.width/2 + sliderLabel.contentSize.width/2, self.frame.origin.y - self.frame.size.height/2 - sliderLabel.contentSize.height/2)];
    }
    return self;
}

-(int)updateValue{
    int intValue = floorf(self.value);
    NSString *newValue = [NSString stringWithFormat:@"%i",intValue];
    [sliderLabel setString:newValue];
    return intValue;
}

-(void)dealloc {
    sliderLabel = nil;
    [sliderLabel release];
    [super dealloc];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
