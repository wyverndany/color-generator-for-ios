//
//  Slider.h
//  Cocos2dColorTester
//
//  Created by Daniel Rodriguez on 4/25/13.
//
//

#import <UIKit/UIKit.h>
#import "cocos2d.h"

@interface Slider : UISlider
{
    CCLabelTTF *sliderLabel;
}

@property (nonatomic, retain) CCLabelTTF *sliderLabel;

-(int)updateValue;

@end
